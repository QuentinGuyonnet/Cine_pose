CREATE DATABASE cine_pose; -- Synthaxe testée et validée des lignes 1 à 11
USE cine_pose;
CREATE TABLE cinemas (nom_cinema VARCHAR (140) NOT NULL PRIMARY KEY UNIQUE, id_criteres TEXT, GPS FLOAT (20)); -- On admet que deux cinemas de Paris n'auront pas le meme nom
CREATE TABLE utilisateurs (nom VARCHAR (140) NOT NULL, prenom VARCHAR (140) NOT NULL, mdp VARCHAR (140), email VARCHAR (50) NOT NULL PRIMARY KEY, points int(10) DEFAULT 0);
CREATE TABLE services (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, intitule VARCHAR (140));
CREATE TABLE commentaires (contenu TEXT, nom_cinema VARCHAR (140), email VARCHAR (50));
CREATE TABLE evenements (id_event INT NOT NULL PRIMARY KEY UNIQUE, evenement VARCHAR (140));
CREATE TABLE ecrans (id_ecrans INT NOT NULL PRIMARY KEY UNIQUE, nrb int (10));
CREATE TABLE fauteuils (id_fauteuils INT NOT NULL PRIMARY KEY UNIQUE, genre VARCHAR (140));
ALTER TABLE commentaires ADD FOREIGN KEY (nom_cinema) REFERENCES cinemas(nom_cinema);
ALTER TABLE commentaires ADD FOREIGN KEY (email) REFERENCES utilisateurs(email);