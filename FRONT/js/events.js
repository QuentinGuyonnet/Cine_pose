function ballet() {
    document.getElementById("ballet").innerHTML = "Ballet";
}

function opera() {
    document.getElementById("opera").innerHTML = "Opéra";
}

function theatre() {
    document.getElementById("theatre").innerHTML = "Théâtre";
}

function concerts() {
    document.getElementById("concerts").innerHTML = "Concerts";
}

function festivals() {
    document.getElementById("festivals").innerHTML = "Festivals";
}

function spectacles() {
    document.getElementById("spectacles").innerHTML = "Spectacles";
}

function cineclub() {
    document.getElementById("cineclub").innerHTML = "Ciné Club";
}

function conferences() {
    document.getElementById("conferences").innerHTML = "Conférences";
}

function standup() {
    document.getElementById("standup").innerHTML = "Stand up";
}

function rencontres() {
    document.getElementById("rencontres").innerHTML = "Rencontres";
}

function seancesdebat() {
    document.getElementById("seancesdebat").innerHTML = "Séances débat";
}

function cinephilo() {
    document.getElementById("cinephilo").innerHTML = "Ciné philo";
}

function seancesspeciales() {
    document.getElementById("seancesspeciales").innerHTML = "Séances spéciales";
}

function autresevents() {
    document.getElementById("autresevents").innerHTML = "Autres événements artistiques";
}

function projections() {
    document.getElementById("projections").innerHTML = "Projections spéciales";
}

function ugcculte() {
    document.getElementById("ugcculte").innerHTML = "UGC Culte";
}

function repasdetente() {
    document.getElementById("repasdetente").innerHTML = "Repas, détente";
}

function lancementproduit() {
    document.getElementById("lancementproduit").innerHTML = "Lancement produit";
}